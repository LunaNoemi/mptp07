import csv
import os
from datetime import date
#Modulo que muestra las opciones
def menu():
    alumnos = {}
    alumnosList=[]
    flag=True
    os.system('cls')
    while flag:
        
        print("1. Registrar alumnos en un diccionario")
        print("2. Mostrar alumnos de una carrera que hicieron el examen ")
        print("3. Mostrar resumen general")
        print("4. Realizar busqueda por DNI")
        print("5. Salir")

        print("Ingrese su opcion")
        op=esNumero()

        if op==5:
            break
        
        ejecutarOpciones(op,alumnos,alumnosList)



def ejecutarOpciones(op, alumnos,alumnosList):
    
    if op == 1:
        os.system('cls')
        leerFichero(alumnos,alumnosList)
        quicksort(alumnosList,0,len(alumnosList)-1)
    elif op == 2:
        os.system('cls')
        mostrarAlumnos(alumnos)
    elif op == 3:
        os.system('cls')
        resumenTotal(alumnos)
    elif op == 4:
        os.system('cls')
        busquedaBinaria(alumnosList)



#Funcion que realiza la lectura del fichero
def leerFichero(alumnos,alumnosList):
    dir = 'mptp07/tp07-Estudiantes.csv'
    with open(dir) as File:
        reader = csv.reader(File)
        cont = 0
        for row in reader:
            lista=row[1].split(";")
            dni=int(lista[1])
            alumnos[dni]=cargarValores(lista,row)
            cont += 1
            alumnosList.append([dni,cargarValores(lista,row)])
    print("Carga exitosa, se ha cargado un total de",cont,"alumnos")


# Punto 1 - Funcion para cargar los valores del diccionario
def cargarValores(lista,row):
    apellido=row[0]
    nombre=lista[0].lstrip()
    carrera=lista[2]

    if lista[3]=='' or lista[3]==" " or lista[3]=="  ":
        lista[3]=False
    notaExamen=lista[3]

    alumno=[apellido, nombre, carrera, notaExamen]
    return alumno

#Funcion para capturar el error al input int
def esNumero():
  while True:
    try:
      numero=int(input(""))
      break
    except :
      print("El valor debe ser NUMERICO")
  return numero


# Punto 1 - Funcion para ordenar la lista de alumnosList para la busqueda binaria
def quicksort(lst, start, stop):
    if stop - start > 0:
        pivot, left, right = lst[start][0], start, stop
        while left <= right:
            while lst[left][0] < pivot:
                left += 1
            while lst[right][0] > pivot:
                right -= 1
            if left <= right:
                lst[left][0:], lst[right][0:] = lst[right][0:], lst[left][0:]
                left += 1
                right -= 1
        quicksort(lst, start, right)
        quicksort(lst, left, stop)
    return lst

# Punto 5 - Busqueda binaria por dni
def busquedaBinaria(lst):
    print("Ingrese el DNI a buscar")
    dni=esNumero()

    izq = 0
    der = len(lst) -1

    while izq <= der:
        medio = (izq + der)//2

        if lst[medio][0] == dni:
            os.system('cls')
            return print("El alumno"," ".join(lst[medio][1][0:2]),"tiene la calificacion de:", "AUSENTE" if not lst[medio][1][3] else lst[medio][1][3] )
        elif lst[medio][0] > dni:
            der = medio-1
        else:
            izq = medio+1
    return print("No se encontro el dni")


#Punto 2 mostrar los alumnos que cursen una carrera dada
# y el promedio de aprobados y desaprobados
def mostrarAlumnos(alumnos):
    carreras()
    carrera=input('Elija una carrera para buscar: ')
    lista=[]
    for  DNI,datos in alumnos.items():
        if (alumnos[DNI][2]==carrera) and (alumnos[DNI][3]!=False):
            print(DNI,datos)
            alumnoGuardar=[DNI,datos[0],datos[1],datos[2],datos[3]]
            lista.append(alumnoGuardar)
    aproYDesap(lista,carrera)
    
def aproYDesap(lista,carrera):
    aprobados=0
    desaprobados=0
    alumnosFichero=[]
    if len(lista)>0:
        for i in lista:
          alumnosFichero.append(i)
          if int(i[4])>=50:
            aprobados+=1
          elif int(i[4])<50:
            desaprobados+=1
        print('Aprobados: ',"{:.3f}".format((aprobados*100)/len(lista)),'%')
        print('Desaprobados: ',"{:.3f}".format((desaprobados*100)/len(lista)),'%')
        crearArchivo(alumnosFichero) 
    else:
        print('La lista de alumnos que cursan "',carrera,'"esta vacia')   
    
def carreras():
    print('''Elija una carrera para iniciar la busqueda
             1)IIND: ingenierÃ­a industrial
             2)IINF: ingenierÃ­a en informÃ¡tica
             3)IQCA: ingenierÃ­a quÃ­mica
             4)IMIN: ingenierÃ­a en minas
             5)LSIS: licenciatura en sistemas''')
             
#punto 4 resumen total
def resumenTotal(alumnos):
    contador=0
    ausentes=0
    desap=0
    apro=0
    for DNI,datos in alumnos.items():
       contador+=1
       if alumnos[DNI][3]==False:
            ausentes+=1
       else:
            if int(alumnos[DNI][3])<50:
                desap+=1
            elif int(alumnos[DNI][3])>=50:
                apro+=1
    mostrar(contador,ausentes,desap,apro)

def mostrar(contador,ausentes,desap,apro):

    print('              RESUMEN TOTAL')
    print('Alumnos registrados ',contador )
    print()
    print('Cantidad de alumnos ausente ',ausentes)  
    print('porsentaje ',"{:.3f}".format((ausentes*100)/contador), '%')
    print('Alumnos desaprobados ',desap)
    print('porsentaje ',"{:.3f}".format((desap*100)/contador), '%')
    print('Alumnos aprobados ',apro)
    print('porsentaje ',"{:.3f}".format((apro*100)/contador), '%')

#Punto -3 Crear el fichero con los alumnos con las condiciones del punto 2
def crearArchivo(alumnosPresentes):
    print("Desea guardar esta lista de alumnos en un nuevo archivo? S/N")
    op=input().upper()

    if op=="S":
        fecha = date.today()
        dia=str(fecha.day)
        mes=str(fecha.month)
        anio=str(fecha.year)
        fechaActual=dia+"-"+mes+"-"+anio
        nombreArchivo="Estudiantes-"+alumnosPresentes[0][3]+"-"+fechaActual+".csv"

        with open(nombreArchivo, 'w', newline='') as file:
            writer = csv.writer(file,delimiter=';')
            writer.writerows(alumnosPresentes)
        os.system("cls")
        print("Se creo el archivo Exitosamente")


#Programa Principal
menu()
